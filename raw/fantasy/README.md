The art in this directory and subdirectories is by [Craft Pix](https://opengameart.org/users/craftpixnet)

Source: [OpenGameArt.Org](https://opengameart.org/)

Licence: [OGA-BY 3.0](http://static.opengameart.org/OGA-BY-3.0.txt)
