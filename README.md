# Game Art

Free game art assets from [OpenGameArt.Org](https://opengameart.org/).

The assets are released under varying Open Source Licences. See individual directories for details.

# Fantasy

Author: [Craft Pix](https://opengameart.org/users/craftpixnet), Licence: [OGA-BY 3.0](http://static.opengameart.org/OGA-BY-3.0.txt)

![](fantasy/characters/archer_female/preview.png) ![](fantasy/characters/fighter_female/preview.png) ![](fantasy/characters/fighter_male_elf/preview.png) ![](fantasy/characters/archer_male_elf/preview.png) ![](fantasy/characters/fairy_blue/preview.png) ![](fantasy/characters/wizard_male_elf/preview.png) ![](fantasy/characters/orc_hammer/preview.png)

# Fish

Authors: [Kenney Vleugels](http://www.kenney.nl), [Scribe](https://opengameart.org/users/scribe), Licence: [Public Domain](http://creativecommons.org/publicdomain/zero/1.0/)

# Platformer

Author: [Kenney Vleugels](http://www.kenney.nl), Licence: [Public Domain](http://creativecommons.org/publicdomain/zero/1.0/)

![](platformer/preview.png)

# Puzzle
Author: [Kenney Vleugels](http://www.kenney.nl), Licence: [Public Domain](http://creativecommons.org/publicdomain/zero/1.0/)

![](puzzle/preview.png)
