# Archer (Female)

Author: [Craft Pix](https://opengameart.org/users/craftpixnet)

Licence: [OGA-BY 3.0](http://static.opengameart.org/OGA-BY-3.0.txt)

Sources:

- [OpenGameArt.Org: Woman Warrior 2D Sprite](https://opengameart.org/content/woman-warrior-2d-sprite)
- [OpenGameArt.Org: Fairy 2D Sprite](https://opengameart.org/content/fairy-2d-sprite)
- [OpenGameArt.Org: Elf 2D Game Sprite](https://opengameart.org/content/elf-2d-game-sprite)
- [OpenGameArt.Org: 2D Game Orcs Sprite](https://opengameart.org/content/2d-game-orcs-sprite)
- [OpenGameArt.Org:
Zombie Punk Character Sprite
](https://opengameart.org/content/zombie-punk-character-sprite)
