# Fantasy Icons

Author: [Craft Pix](https://opengameart.org/users/craftpixnet)

Licence: [OGA-BY 3.0](http://static.opengameart.org/OGA-BY-3.0.txt)

Sources:

- [OpenGameArt.Org: 20 Medieval Icons](https://opengameart.org/content/20-medieval-icons)
- [OpenGameArt.Org: Game Icons Knight Armor](https://opengameart.org/content/game-icons-knight-armor)
- [OpenGameArt.Org: Game Icons Fantasy things](https://opengameart.org/content/game-icons-fantasy-things)
- [OpenGameArt.Org: Game Icons of Fantasy Potions Daggers](https://opengameart.org/content/game-icons-of-fantasy-potions-daggers)
- [OpenGameArt.Org: Game Icons of Fantasy Potions Pack 1](https://opengameart.org/content/game-icons-of-fantasy-potions-pack-1)
- [OpenGameArt.Org: Game Icons of Fantasy Mage Outfit](https://opengameart.org/content/game-icons-of-fantasy-mage-outfit)
